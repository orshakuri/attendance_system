<?php
require_once("../Models/Course/Course.php"); //קורס
require_once("../Models/Trend/Trend.php");	//מגמה
require_once("../Account/Account.php"); //משתמש
require_once("../Models/Student/Student.php"); //סטודנט
require_once("../Models/Lecture/Lecture.php"); //הרצאה
require_once("../Models/Classroom/Classroom.php"); //כיתה
require_once("../Models/Semester/Semester.php"); //סמסטר
require_once("../Models/StudyYear/StudyYear.php"); //שנת לימודים
require_once("../Models/LectureTime/LectureTime.php");//זמן הרצאה
class dbClass
{
	private $host;
	private $db;
	private $charset;
	private $user;
	private $pass;
	private $opt = array(
	        PDO::ATTR_ERRMODE  =>PDO::ERRMODE_EXCEPTION,
	        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC);
	private $connection;
	
	public function __construct(string $host = "localhost" , string $db = "attendance_system_db" , 
	                            string $charset = "utf8" , string $user = "root" , string $pass = "" )
								{
									$this->host = $host;
									$this->db = $db;
									$this->charset = $charset;
									$this->user = $user;
									$this->pass = $pass;
								}
	
	
	private function connect()//function connect to DB
	{
		$dsn = "mysql:host=$this->host;dbname=$this->db;charset=$this->charset";
		$this->connection = new PDO($dsn,$this->user,$this->pass,$this->opt);
		
	}
	
	
	public function disconnect()//function disconnect from DB
	{
		$this->connection = null;
	}
	
	
	public function checkVerify($id,$password)//function gets id and password and by SQL query Checks in the database if the information is correct and than returns permission of looged in 
    {
		
        $this->connect();
		
		$statement=$this->connection->query("SELECT * FROM account WHERE id='$id'");

		$my_account=$statement->fetchObject('account');
		
		$hashed_pass=$my_account->getPassword();
		
		if(password_verify($password,$hashed_pass))
		{
			$permission=$my_account->getPermission();
		}
		else 
		{
			$permission=null;
		}
        
		$this->disconnect();
        return $permission;
		
    }

	public function getStudentById($id):Student//function gets student id and  returns  the student as Object(type Student)
	{		
		$this->connect();
		$statement=$this->connection->query("SELECT * FROM student WHERE id='$id'");
		$statement->execute([':id'=>$id]);
		$result=$statement->fetchObject('student');
		$this->disconnect();
		return $result;
	}
	
	
	public function requstedStudents()//function returns array of all the student table
    {
        $this->connect();
        $studentsArrayId=array();
        $result = $this->connection->query("SELECT id FROM student");
        while($row=$result->fetch(PDO::FETCH_ASSOC))
        {
            $studentsArrayId[]=$row;   
        }
        $this->disconnect();
        return $studentsArrayId;
    }

		public function getOutreStudents()//fucntion returns array of all the outre students by SQL query(Student_Object->isOutre=1)
    {
        $this->connect();
        $studentsArray=array();
        $result = $this->connection->query("SELECT id FROM student WHERE isOutre=1");
        while($row=$result->fetch(PDO::FETCH_ASSOC))
        {
            $studentsArray[]=$row;   
        }
        $this->disconnect();
        return $studentsArray;
    }
	
    public function deleteStudent($id)//function get id of student and by SQL query delete the student
    {
        $this->connect();

        $result = $this->connection->query("DELETE FROM student WHERE id = '$id'");
		
		$this->disconnect();
        
    }
    
    public function setUserSession($id,$ip)
    {

        $this->connect();
        $this->connection->query("UPDATE account SET ip_addr = '$ip' , status=1 WHERE id= '$id'");
     
        $this->disconnect();
    }
    
    public function deleteAccount($id)//function get id of account and by SQL query delete the account
    {
        $this->connect();

        $result = $this->connection->query("DELETE FROM account WHERE id = '$id'");
		
		$this->disconnect();
    }
    
	//function gets all students fields and by SQL query ADDs new student
    public function insertStudent($student_id,$student_fname,$student_lname,$student_email,$student_mobile,$student_classID)
    {
        $this->connect();
        
        $this->connection->query("INSERT INTO student(id,first_name,last_name,email,mobile,class_id)
                                                                VALUES('$student_id'
                                                                      ,'$student_fname'
                                                                      ,'$student_lname'
                                                                      ,'$student_email'
                                                                      ,'$student_mobile'
                                                                      ,'$student_classID')");
                                                                      
        $this->disconnect();
    }
    
    public function update_isOutre($student_id)//function gets student id and update the field isOutre to 0 by SQL query
    {
        $this->connect();
        $this->connection->query("UPDATE student SET isOutre = 0 WHERE id= '$student_id'");
                                                    
                  
        $this->disconnect();
    }
        public function update_test($tmp)//function gets student id and update the field isOutre to 0 by SQL query
    {
        $this->connect();
        $this->connection->query("UPDATE account SET name = '$tmp' WHERE id= 5");
                                                    
                  
        $this->disconnect();
    }
	
	public function getEmailByID($id)//function gets student id and returns his email
	{
		$this->connect();
		$student=$this->getStudentById($id);	
		$this->disconnect();
		
		return $student->getEmail();
	}
	
	public function getOutersStudentsArray()//function returns array of all the outres students by SQL query
	{
		$this->connect();
		$studentsArray=array();
		
		$result=$this->connection->query("SELECT * FROM student WHERE isOutre=1");
			
		while($row=$result->fetchObject('Student')){
		$studentsArray[]=$row;
		}
			
		$this->disconnect();
	return $studentsArray;
	}
	
	public function studentsArrayToString($arrayOfStudents)//function gets students array and returns the students deatails in string 
    {
        $str="";
		  foreach($arrayOfStudents as $key=>$value){
              $str.= $value->toString().PHP_EOL;
		  }
		return $str;
	}
		
    public function insertCourse($acc_id,$trend_id,$subject,$study_hours){
        $this->connect();
        $this->connection->query("INSERT INTO course(id, trend_id, subject, study_hours)
                                                                VALUES('$acc_id'
                                                                      ,'$trend_id'
                                                                      ,'$subject'
                                                                      ,'$study_hours')");
        
        $this->disconnect();
    }
    public function getAccIdByName($name){
        $this->connect();
		$statement=$this->connection->query("SELECT id FROM account WHERE name='$name'");
        $id=$statement->fetch(PDO::FETCH_ASSOC);
		
		$this->disconnect();
		return $id['id'];
    }
    
        public function getAccNameById($id){
        $this->connect();
		$statement=$this->connection->query("SELECT name FROM account WHERE id='$id'");
        $name=$statement->fetch(PDO::FETCH_ASSOC);
		
		$this->disconnect();
		return $name['name'];
    }
    
        public function getAccountsArray(){
        $this->connect();
		$statement=$this->connection->query("SELECT * FROM account");
        if($statement == null){
            return 'error';
        }          
        $acc_arr = array();
        $i=0;
            while($row=$statement->fetchObject('Account')){
                $acc_arr[$i++]=$row;
            }	
             
		$this->disconnect();
		return $acc_arr;
    }
    
    
    public function getTrendIdByName($name){
        $this->connect();
		$statement=$this->connection->query("SELECT id FROM trend WHERE trend_name='$name'");
        $id=$statement->fetch(PDO::FETCH_ASSOC);
		
		$this->disconnect();
		return $id['id'];
    }
    
	public function getStudentsClassListByClassId($class_id)//function gets class id and returns array of all the outres students at the class
	{
		$this->connect();
		$studentsArray=array();
		$result=$this->connection->query("SELECT * FROM student WHERE class_id='$class_id' AND isOutre=1");
			
		while($row=$result->fetchObject('Student')){
			$studentsArray[]=$row;
		}
			
		$this->disconnect();
		return $studentsArray;
	}
	
		public function getClassList()//function returns id's of all the classes
	{
		$this->connect();
		$class_id_arr=array();
		
		$result=$this->connection->query("SELECT id FROM classroom");
			
		while($row=$result->fetch(PDO::FETCH_ASSOC))
		{
		$class_id_arr[]=$row;
		}
			
		$this->disconnect();
	return $class_id_arr;
	}

		public function getOutersStudentsIdArray()//function returns array of all the outres students
	{
		$this->connect();
		$studentsArray=array();
		
		$result=$this->connection->query("SELECT id FROM student WHERE isOutre=1");
			
		while($row=$result->fetch(PDO::FETCH_ASSOC))
		{
		$studentsArray[]=$row;
		}
			
		$this->disconnect();
		
	return $studentsArray;
	}
		public function sendMail($mystudent){//function gets object type Student and sending an "alarm mail"
		$to=$mystudent->getEmail();
		
		$subject="Notification mail";
		$name =$mystudent->getFirst_Name()." ".$mystudent->getLast_Name();
		$message = "Hello ".$name.","
		.PHP_EOL ."we just wanted to let you know that you were found outre in the attendance system"
		.PHP_EOL ."Pay attention if you lose three more lecturs in this semester you wont be able to take a exams"
		.PHP_EOL . PHP_EOL . PHP_EOL ."Best regards, Technion national practical engineering school.";
		
		$headers='From:Technion national practical engineering school';
		
		mail($to,$subject,$message,$headers);
		
		}


    public function insert_test($array)
    {
        $this->connect();

        $this->connection->query("INSERT INTO test_table(id,times,lecturers)
                                                                VALUES('$array[1]','$array[6]','$array[7]')");
                                                                      
        $this->disconnect();
    }
   
}

?>
