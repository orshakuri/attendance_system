<?php
include('../View/header.php');
include('../Core/Functions.php');
require '../Core/dbClass.php';
$db = new dbClass();
$accountsArray=$db->getAccountsArray();

?>

    <!DOCTYPE html>
    <html dir="rtl">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>

    <body>

        <h1>רשימת משתמשים</h1>

        <form action="admin_controller.php" method="post">
            <table>

                <thead>
                    <tr>
                        <th>תעודת זהות</th>
                        <th>שם מלא</th>
                        <th>כתובת דואר אלקטרוני</th>
                        <th>סוג הרשאה</th>
                    </tr>
                </thead>

                <tbody>
                    <?php

                foreach($accountsArray as $account)
                {
                ?>
                        <tr>
                            <td>
                                <?php echo $account->getId(); ?> </td>
                            <td>
                                <?php echo $account->getName(); ?> </td>
                            <td>
                                <?php echo $account->getMail(); ?> </td>
                            <td>
                                <?php echo $account->getPermission(); ?> </td>
                            <td>

                                <?php
          
                            $id = $account->getId();
                                                      
                      echo      "<input type='hidden' name='del'  value='$id' </input>";
                                                
                         ?>
                                    <button  onclick="return  confirm('אתה עומד למחוק את המשתמש (איכשהו להכניס שם משתמש) האם אתה בטוח?')">מחק משתמש </button>
                            </td>
                        </tr>
                        <?php } ?>


                </tbody>

            </table>
        </form>


<!--        <button href="admin_addAcc.php">הוסף משתמש</button>-->
<a href="admin_addAcc.php"> ADD</a>



        <?php
include('../View/footer.php');
     
?>

    </body>

    </html>
