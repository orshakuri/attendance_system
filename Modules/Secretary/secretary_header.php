<?php

?>


<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="secretary_startSemester.php">התחל סמסטר</a></li>
                    <li><a href="secretary_EditSemesterInfo.php">עדכון נתוני סמסטר</a></li>
                    <li><a href="secretary_reportsExport.php">הפקת דוחות</a></li>
                    <li><a href="secretary_attendanceManage.php">ניהול נוכחות</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-left">
                    <li><a><?php echo $_SESSION['user_name'];?></a></li>
                </ul>
            </div>
        </div>
    </nav>

    

</body>
</html>
