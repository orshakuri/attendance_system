<?php
//Student class fields initialize ,getters and setters
class Student
{                                                                                                                                                                            
	private $id;  
	private $first_name; 
	private $last_name;  
	private $email;
	private $mobile;  
	private $class_id;
	
	
	
	public function getId()
	{
		return $this->id;
	}
	
	public function setId($id)
	{
		$this->id = $id;
	}
	
	
	public function getFirst_Name()
	{
		return $this->first_name;
	}
	
	public function setFirst_Name($first_name)
	{
		$this->first_name = $first_name;
	}
	
	
	public function getLast_Name()
	{
		return $this->last_name;
	}
	
	public function setLastName($last_name)
	{
		$this->last_name = $last_name;
	}
	
	
	public function getEmail()
	{
		return $this->email;
	}
	
	public function setEmail($email)
	{
		$this->email = $email;
	}

	
	public function getMobile()
	{
		return $this->mobile;
	}
	
	public function setMobile($mobile)
	{
		$this->mobile = $mobile;
	}

	
	public function getClass_Id()
	{
		return $this->class_id;
	}
	
		public function setClass_Id($class_id)
	{
		 $this->class_id=$class_id;
	}	
	
	public function toString()
	{
		return "ID:".$this->id.
		"  First-Name:".$this->first_name .
		"  Last-Name:".$this->last_name .
		"  Email:".$this->email.
		"  Phone-Number:".$this->mobile.
		"  Class: ".$this->class_id;
	}
	
}

?>