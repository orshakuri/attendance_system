<?php
//Trend class fields initialize ,getters and setters
class Trend
{                                                                                                                                                                            
	private $id;
    private $coordinator_id;
    private $semester_id;
	private $trend_name; 

	
	public function getId()
	{
		return $this->id;
	}
	
	public function setId($id)
	{
		$this->id = $id;
	}
	
    public function getCoordinator_id()
	{
		return $this->coordinator_id;
	}
	
	public function setCoordinator_id($coordinator_id)
	{
		$this->coordinator_id = $coordinator_id;
	}
    
    public function getSemester_Id()
	{
		return $this->semester_id;
	}
	
	public function setSemester_id($semester_id)
	{
		$this->semester_id = $semester_id;
	}
	
	public function getTrend_name()
	{
		return $this->trend_name;
	}
	
	public function setTrend_name($trend_name)
	{
		$this->trend_name = $trend_name;
	}
	
	
}

?>