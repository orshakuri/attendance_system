<?php
//Course class fields initialize ,getters and setters
class Course
{                                                                                                                                                                            
	private $id;  
	private $subject; 
    private $trend_id;
    private $study_hours;


	public function getId()
	{
		return $this->id;
	}
	
	public function setId($id)
	{
		$this->id = $id;
	}
	
	
	public function getSubject()
	{
		return $this->subject;
	}
	
	public function setSubject($subject)
	{
		$this->subject = $subject;
	}
    
    	public function getTrend_id()
	{
		return $this->trend_id;
	}
	
	public function setTrend_id($trend_id)
	{
		$this->trend_id = $trend_id;
	}
    
    	public function getStudy_hours()
	{
		return $this->study_hours;
	}
	
	public function setStudy_hours($study_hours)
	{
		$this->study_hours = $study_hours;
	}
    
    public function calcLecturerHours($timeArray){
        $sum=0;
        $timeArray=(str_split($timeArray, 15));
        foreach($timeArray as $v){
            $Ltime = new LectureTime($v);
            $Ltime->calcTotalHours();
            $sum+=$Ltime->getTotal_Hours();
        }
        return $sum*7;
    }
        
    public function insert($arr){
        $db = new dbClass();
        $name = str_replace("'","*",$arr[7]);
        $this->id = $db->getAccIdByName($name);
        $res = explode(" ",$arr[5]);
        $this->trend_id = $db->getTrendIdByName($res[0]);
        $this->subject = $arr[2];
        $this->study_hours = $this->calcLecturerHours($arr[6]);
       $db->insertCourse($this->id,$this->trend_id,$this->subject,$this->study_hours);
    }

	
}

?>
