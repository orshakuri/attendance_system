<?php
//StudyYear class fields initialize ,getters and setters
class StudyYear
{                                                                                                                                                                            
	private $id;  
	private $year;
 

	public function getId()
	{
		return $this->id;
	}
	
	public function setId($id)
	{
		$this->id = $id;
    }  
	   
    public function getYear()
	{
		return $this->year;
	}
	
	public function setYear($year)
	{
		$this->year = $year;
	}
     
	
}

?>
