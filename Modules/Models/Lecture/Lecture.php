<?php
//Lecture class fields initialize ,getters and setters

	class Lecture
	{
		private $id;
		private $date;
		private $time;
		private $course_id;
		private $lecture_comments;
		
		public function getLectureId()
		{
			return $this->id;
		}
		
		public function setLectureId($id)
		{
			$this->id=$id;
		}
		
		public function getLectureDate()
		{
			return $this->date;
		}
		
		public function setLectureDate($date)
		{
			$this->date=$date;
		}
		
		public function getLectureTime()
		{
			return $this->time;
		}
		
		public function setLectureTime($time)
		{
			$this->time=$time;
		}
		
		public function getLectureCourseId()
		{
			return $this->course_id;
		}
		
		public function setLectureCourseId($course_id)
		{
			$this->course_id=$course_id;
		}
        
        public function getLectureComments()
		{
			return $this->lecture_comments;
		}
		
		public function setLectureComments($lecture_comments)
		{
			$this->lecture_comments=$lecture_comments;
		}
	}
?>