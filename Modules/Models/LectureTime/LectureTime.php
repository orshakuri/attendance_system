<?php
//LectureTime class fields initialize ,getters and setters
class LectureTime
{                                                                                                                                                                            
	private $lecture_day;
	private $start_hour;
    private $start_min;
    private $end_hour;
    private $end_min;
    private $total_hours;
    
    public function __construct($str){
    $tav= "'";
    $arr=explode("-",str_replace($tav,"-",$str));
    $start_arr=explode(":",$arr[1]);
    $end_arr=explode(":",$arr[2]);
    $this->lecture_day=$arr[0];
    settype($start_arr[0],"int");
    settype($start_arr[1],"int");
    settype($end_arr[0],"int");
    settype($end_arr[1],"int");
    $this->start_hour=$start_arr[0];
    $this->start_min=$start_arr[1];
    $this->end_hour=$end_arr[0];
    $this->end_min=$end_arr[1];    
}
	
	public function getLecture_Day()
	{
		return $this->lecture_day;
	}
	
	public function setId($lecture_day)
	{
		$this->lecture_day = $lecture_day;
    }  
	   
    public function getStart_Hour()
	{
		return $this->start_hour;
	}
	
	public function setStart_Hour($start_hour)
	{
		$this->start_hour = $start_hour;
	}
     
	public function getStart_Min()
	{
		return $this->start_min;
	}
	
	public function setStart_Min($start_min)
	{
		$this->start_min = $start_min;
	}
    
    public function getEnd_Hour()
	{
		return $this->end_hour;
	}
	
	public function setEnd_Hour($end_hour)
	{
		$this->end_hour = $end_hour;
	}
     
	public function getEnd_Min()
	{
		return $this->end_min;
	}
	
	public function setEnd_Min($end_min)
	{
		$this->end_min = $end_min;
	}
	
    public function getTotal_Hours()
	{
		return $this->total_hours;
	}
	
	public function setTotal_Hours($total_hours)
	{
		$this->total_hours = $total_hours;
	}
    public function toString(){ 
        $a="";
        $b="";
        $c="";
        $d="";
        
        if($this->start_hour<10)
            $a='0';
        if($this->start_min<10)
            $b='0';
        if($this->end_hour<10)
            $c='0';
        if($this->end_min<10)
            $d='0';
        
        return $a.$this->start_hour.":".$this->start_min.$b."-".$c.$this->end_hour.":".$this->end_min.$d."-".$this->lecture_day."<br>";
    }
    
	public function calcTotalHours(){
        if(isset($this->end_hour))
        {
            $this->total_hours=(integer)(  ((($this->end_hour - $this->start_hour)*60) + ($this->end_min - $this->start_min) )  / 45);
        }
    }
    
    
}

?>
