<?php
//Acount class fields initialize ,getters and setters

class Account
{                                                                                                                                                                            
	private $id;  
	private $name; 
    private $mail;
	private $permission;  
	private $password;

	
	
	public function getId()
	{
		return $this->id;
	}
	
	public function setId($id)
	{
		$this->id = $id;
	}
	
	
	public function getName()
	{
		return $this->name;
	}
	
	public function setName($name)
	{
		$this->name = $name;
	}
	
	
	public function getPermission()
	{
		return $this->permission;
	}
	
	public function setPermission($permission)
	{
		$this->permission = $permission;
	}

	public function getPassword()
	{
		return $this->password;
	}
	
	public function setPassword($password)
	{
		$this->password = $password;
	}	
    
    public function getMail()
	{
		return $this->mail;
	}
	
	public function setMail($mail)
	{
		$this->mail = $mail;
	}
	
	
	
	
}

?>