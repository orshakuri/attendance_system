<!DOCTYPE html>
<html dir="rtl">

<head>
    <link rel="stylesheet" type="text/css" href="../login_style.css">
    <title>דף התחברות מערכת לניהול נוכחות</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>

    <center>

        <div class="container" >

            <h1>מערכת  לניהול נוכחות</h1>
            <h2>בית הספר הארצי להנדסאים טכניון</h2>
            <br>

            <form action="login_controller.php" method="post">

                <div class="form-group has-feedback">
                    <input type="number" class="form-control" id="user_id" placeholder="ת.ז (ספרות בלבד)" name="id">
                    <i class="glyphicon glyphicon-user form-control-feedback"></i>
                    
                </div>
                

                <div class="form-group has-feedback">
                    <input type="password" class="form-control" id="user_pwd" placeholder="סיסמא" name="pwd" >
                    <i class="glyphicon glyphicon-lock form-control-feedback"></i>
                </div>

                <button type="submit" class="btn btn-default">כניסה למערכת</button>
            </form>


            <hr>

            <header>משתמש לא רשום? <br> להרשמה יש לגשת למזכירות.</header>

        </div>
        </center>

    

</body>

</html>

